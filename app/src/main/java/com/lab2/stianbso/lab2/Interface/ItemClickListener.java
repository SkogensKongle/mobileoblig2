package com.lab2.stianbso.lab2.Interface;

import android.view.View;

/**
 * Created by Stian on 16.03.2018.
 */

public interface ItemClickListener {
    void onClick(View view, int position, boolean isLongClick);
}
