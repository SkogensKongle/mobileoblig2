package com.lab2.stianbso.lab2.Model;

import java.util.List;

/**
 * Created by Stian on 16.03.2018.
 */

public class RSSObject {
    public String status;
    public Feed feed;
    public List<Item> items;

    public RSSObject(String status, Feed feed, List<Item> items){

    }

    public String getStatus(){
        return status;
    }

    public void setStatus(String status){
        this.status = status;
    }

    public  Feed getFeed(){
        return feed;
    }

    public void setFeed(Feed feed){
        this.feed = feed;
    }

    public List<Item> getItems(){
        return items;
    }

    public void setItems(List<Item> items){
        this.items = items;
    }
}
